using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    [SerializeField]
    private  float _speed = 0.1f;
    private Vector2 _startPos;

    private void Update()
    {
        if (Input.touchCount > 0)
        {
            var touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    _startPos = touch.position;
                    break;

                case TouchPhase.Moved:
                    var dir = touch.position - _startPos;
                    _startPos = touch.position;

                    var pos = transform.position + new Vector3(-dir.x, 0f, 0f);
                    transform.position = Vector3.Lerp(transform.position, pos, Time.deltaTime * _speed);
                    break;
            }
        }
    }


}