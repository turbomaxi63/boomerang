﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Boomerang : MonoBehaviour
{
    [SerializeField]
    private DOTweenAnimation DTRotate;
    [SerializeField]
    private DOTweenAnimation DTPlay;
    public bool fly;
    public void startFly()
    {
       fly = true;
        DTRotate.DOPlay();
        DTPlay.DORestart();       
    }
    public void EndFly()
    {
       fly = false;
        DTRotate.DOPause();
        BoomerangControler.Instance.AddBoomerang();
    }
}
