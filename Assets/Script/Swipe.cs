﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swipe : MonoBehaviour
{
    public static Swipe instance;
    public enum direction { Left,Right,Up,Down};
    bool[] swipe = new bool[4];
    Vector2 startTouch;
    bool touchMoved;
    Vector2 swipeDelta;
    const float SWIPE_THRESHOLD = 50;
    Vector2 TouchPosition() { return (Vector2)Input.mousePosition; }
    bool TouchBegan() { return Input.GetMouseButtonDown(0); }
    bool TouchEnded() { return Input.GetMouseButtonDown(0); }
    bool GetTouch() { return Input.GetMouseButtonDown(0); }


    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {//START
        if (TouchBegan())
        {
            startTouch = TouchPosition();
            touchMoved = true; ;
        }
        else if (TouchEnded() && touchMoved == true)
        {
            SendSwipe();
            touchMoved = false;
        }
        //CALC DISTANCE
        swipeDelta = Vector2.zero;
        if (touchMoved && GetTouch())
        {
            swipeDelta = TouchPosition() - startTouch;
        }
        //CHECK SWIPE
        if (swipeDelta.magnitude > SWIPE_THRESHOLD)
        {
            if (Mathf.Abs(swipeDelta.x) > Mathf.Abs(swipeDelta.y))
            {
                //LEFT/RIGHT
                swipe[(int)direction.Left] = swipeDelta.x < 0;
                swipe[(int)direction.Right] = swipeDelta.x > 0;
            }
            else
            {
                //UP/DOWN
                swipe[(int)direction.Down] = swipeDelta.y < 0;
                swipe[(int)direction.Up] = swipeDelta.y > 0;

            }
            SendSwipe();
        }
    }
    void SendSwipe()
    {
        if (swipe[0] || swipe[1] || swipe[2] || swipe[3])
        {
            Debug.Log(swipe[0] + "|" + swipe[1]+"|" + swipe[2]+"|" + swipe[3]);
        }
        else
        {
            Debug.Log("Click");
        }
    }
    private void Reset()
    {
        startTouch = swipeDelta = Vector2.zero;
        touchMoved = false;
        for (int i = 0; i<4; i++) { swipe[i] = false; }

    }
}

