﻿using UnityEngine;
using System.Collections;

namespace FunnyBlox
{
    public class CameraFollow :MonoBehaviour 
    {
        public Transform target;
        public float smoothing = 5f;

        private Transform _transform;
        private Vector3 _targetCamPos;
        private Vector3 offset;


        void OnEnable()
        {
            // Calculate initial offset.
            _transform = transform;

            offset = _transform.position - target.position;
        }
        
        void LateUpdate()
        {
            _targetCamPos = target.position + offset;

            _transform.position = Vector3.Lerp(_transform.position, _targetCamPos, smoothing * Time.deltaTime);
        }

        public void SetPosition()
        {
            _transform.position= target.position + offset;
        }
    }
}
