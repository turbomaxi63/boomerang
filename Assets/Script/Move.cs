﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public enum EgameState
{
    menu = 0,
    game = 1,
    gameover = 2,
    start = 3,
    finish = 4,
}
public class Move : MonoBehaviour
{
    public float speed = 6.0F;
    private Vector3 moveDirection = Vector3.zero;   
    Rigidbody rb;
    public EgameState gameState;
    [SerializeField]
    private GameObject gameOverText;
    [SerializeField]
    private GameObject gameStartText;
    [SerializeField]
    private GameObject gameFinishText;
    [SerializeField]
    private float _speed = 0.1f;
    private Vector2 _startPos;
    [SerializeField]
    private Boomerang boomerang;
    [SerializeField]
    private AnimationGirl WalkGirl;
    [SerializeField]
    private float minXpoz;
    [SerializeField]
    private float maxXpoz;
    [SerializeField]
    private Transform OriginalFlyBoomerang;
    private Transform TempFlyBoomerang;
    private List<Transform> FlyBoomerangs;
    private Transform ParentFlyBoomerang;

    /* [SerializeField]
     private AnimationGirl Attack;*/
    void Start()
    {
        gameState = EgameState.menu;
        rb = GetComponent<Rigidbody>();
        LightingSettings lightingSettings = new LightingSettings();
        lightingSettings.lightProbeSampleCountMultiplier = 1;
            FlyBoomerangs = new List<Transform>();

#if UNITY_IOS || UNITY_ANDROID
        Application.targetFrameRate = 60;
#endif
    }
    void Update()
    {
        if (gameState == EgameState.menu)        {
            
            if (Input.GetMouseButtonDown(0) || (Input.GetKeyDown(KeyCode.Space)))
            {
                gameState = EgameState.start;
            }
        }
        else if (gameState == EgameState.start)
        {
            gameStartText.SetActive(true);

            if (Input.GetMouseButtonDown(0) || (Input.GetKeyDown(KeyCode.Space)))
            {
                
                gameState = EgameState.game;
                gameStartText.SetActive(false);
            }
        }
        else if (gameState == EgameState.game)       {
            
            rb.velocity = new Vector3(0.0f, 0.0f, -speed);            
            WalkGirl.RunAnimation();
            if (transform.position.x < minXpoz)
            {
                transform.position = new Vector3(minXpoz, transform.position.y, transform.position.z);
            }
            else if (transform.position.x > maxXpoz)
            {
                transform.position = new Vector3(maxXpoz, transform.position.y, transform.position.z);
            }
            if (Input.touchCount > 0)
            {
                var touch = Input.GetTouch(0);
                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        _startPos = touch.position; break;
                    case TouchPhase.Moved:
                        var dir = touch.position - _startPos;
                        _startPos = touch.position;
                        var pos = transform.position + new Vector3(-dir.x, 0f, 0f);
                        transform.position = Vector3.Lerp(transform.position, pos, Time.deltaTime * _speed);
                        break;
                }
            }
        }
        else if (gameState == EgameState.gameover || gameState == EgameState.finish)
        {
            if (Input.GetMouseButtonDown(0) || (Input.GetKeyDown(KeyCode.Space)))
                SceneManager.LoadScene(0);
        }
    }
    private void CreateFlyBoomerang()
    {
        TempFlyBoomerang = Instantiate(OriginalFlyBoomerang);
        TempFlyBoomerang.parent = ParentFlyBoomerang;
        TempFlyBoomerang.localPosition = Vector3.zero;
    }
    private void FixedUpdate()
    {
        if (gameState == EgameState.game)
        {
            rb.velocity = new Vector3(0.0f, 0.0f, -speed);           
            if (Input.GetMouseButtonUp(0) || (Input.GetKeyUp(KeyCode.Space)))
            {            
                              
               if (!boomerang.fly)
                {
                    boomerang.startFly();
                }
                
                    BoomerangControler.Instance.RemoveBoomerang();                    
                
            }
        }

    }
    void OnCollisionEnter(Collision other)
    {
        if (gameState == EgameState.game)
        {
            if (other.collider.CompareTag("Boom"))
            {
                EndGame(false);
                Debug.Log("Crash");
            }
        }
    }
    [SerializeField]
    private ParticleSystem GameOverCrashParticles;
    void EndGame(bool win)
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = new Vector2(0f, 0f);
        rb.isKinematic = true;
        if (win)
        {
            gameState = EgameState.finish;
            gameFinishText.SetActive(true);
        }
        else
        {
            gameState = EgameState.gameover;
            gameOverText.SetActive(true);
            GameOverCrashParticles.transform.position = transform.position;
            GameOverCrashParticles.Play();           
            //WalkGirl.enabled = false;
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (gameState == EgameState.game)
        {
            if (other.CompareTag("Finish"))

            {
                EndGame(true);
                //WalkGirl.enabled = false;

            }


        }
    }
}
