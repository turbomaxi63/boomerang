using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class BoomerangControler : MonoSingleton<BoomerangControler>
{
    [SerializeField]
    private Transform BoomerangOriginal;
    [SerializeField]
    private Transform ParentBoomerang;
    [SerializeField]
    private int AmountBoomerang;
    private Transform TempBoomerang;
    [SerializeField]
    private float HeightBoomerang = 0f;
    private List<Transform> Boomerangs;

    void Start()
    {
        Boomerangs = new List<Transform>();
    }
    public void AddBoomerang()
    {
        AmountBoomerang++;
        TempBoomerang = Instantiate(BoomerangOriginal);
        TempBoomerang.parent = ParentBoomerang;
        TempBoomerang.localPosition = Vector3.zero;
        TempBoomerang.Translate(0, AmountBoomerang * HeightBoomerang, 0);
        Boomerangs.Add(TempBoomerang);
    }
    public void RemoveBoomerang()
    {       
            if (AmountBoomerang > 0)
            {
                AmountBoomerang--;
                TempBoomerang = Boomerangs.Last();
                Boomerangs.Remove(TempBoomerang);
                Destroy(TempBoomerang.gameObject);
            }        
    }
    //public void DestroyBoomerang()
    //{
    //    if (AmountBoomerang > 0)
    //    {

            
    //    }
    //}
    public bool HideUpper()
    {
        for (int i = Boomerangs.Count - 1; i >= 0; i--)
        {
            if (Boomerangs[i].gameObject.activeSelf)
            {
                Boomerangs[i].gameObject.SetActive(false);
                return true;
            }
        }
        return false;
    }
    public void ShowUpper()
    {
        for (int i = 0; i < Boomerangs.Count; i++)
        {
            if (!Boomerangs[i].gameObject.activeSelf)
            {
                Boomerangs[i].gameObject.SetActive(true);
                break;
            }
        }
    }


}
