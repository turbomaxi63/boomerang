using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainBost : MonoBehaviour
{
    Rigidbody rb;
    //���������� 
    [SerializeField]
    private Transform nowPlayer;
    //[SerializeField]
    //private float increaseSize = 0.0f;
    //[SerializeField]
    //private float standartSize = 0.0f;
    //[SerializeField]
    //private float MaxSize = 0.0f;
    ////����������
    //[SerializeField]
    //private float decreaseSizesaw = 0.0f;



    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    [SerializeField]
    private ParticleSystem cutting;
    private ParticleSystem particleCutWood;
    [SerializeField]
    private ParticleSystem BoostIncrease;
    private ParticleSystem ParticleBoostincrease;
    [SerializeField]
    private ParticleSystem StoneDecrease;
    private ParticleSystem ParticleBoosDecrease;
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Increase"))
        {
            BoomerangControler.Instance.AddBoomerang();
            Vector3 localScale = nowPlayer.localScale;
            Destroy(other.gameObject);                        
            other.transform.parent = null;
            ParticleBoostincrease = Instantiate<ParticleSystem>(BoostIncrease);
            ParticleBoostincrease.transform.position = new Vector3(other.transform.position.x, other.transform.position.y, other.transform.position.z);
            ParticleBoostincrease.Play();
        }
        if (other.CompareTag("Decrease"))
        {
            BoomerangControler.Instance.RemoveBoomerang();
            Vector3 localScale = nowPlayer.localScale;           
            other.transform.parent = null;
            ParticleBoosDecrease = Instantiate<ParticleSystem>(StoneDecrease);
            ParticleBoosDecrease.transform.position = new Vector3(other.transform.position.x, other.transform.position.y, other.transform.position.z);
            ParticleBoosDecrease.Play();
            Destroy(other.gameObject);
        }

        //if (other.CompareTag("Destroy"))
        //{
        //    BoomerangControler.Instance.DestroyBoomerang();
        //}

        if (other.CompareTag("Boom"))
        {
            other.transform.parent = null;
            particleCutWood = Instantiate<ParticleSystem>(cutting);
            particleCutWood.transform.position = new Vector3(other.transform.position.x, other.transform.position.y, other.transform.position.z);
            particleCutWood.Play();
            Rigidbody rb = other.GetComponent<Rigidbody>();
            if (rb)
            {
                rb.isKinematic = false;
            }
            Destroy(other.gameObject);
        }
        //if (other.CompareTag("Increase"))
        //{
        //    Vector3 localScale = nowPlayer.localScale;
        //    if (MaxSize >= localScale.x)
        //    {
        //        transform.localScale += new Vector3(increaseSize, increaseSize, increaseSize);
        //    }
        //    other.transform.parent = null;
        //    ParticleBoostincrease = Instantiate<ParticleSystem>(BoostIncrease);
        //    ParticleBoostincrease.transform.position = new Vector3(other.transform.position.x, other.transform.position.y, other.transform.position.z);
        //    ParticleBoostincrease.Play();
        //    if (MaxSize > standartSize)
        //    {
        //        Destroy(other.gameObject);
        //    }
        //}
        //if (other.CompareTag("Decrease"))
        //{
        //    Vector3 localScale = nowPlayer.localScale;
        //    transform.localScale = new Vector3(standartSize, standartSize, standartSize);
        //    other.transform.parent = null;
        //    ParticleBoosDecrease = Instantiate<ParticleSystem>(StoneDecrease);
        //    ParticleBoosDecrease.transform.position = new Vector3(other.transform.position.x, other.transform.position.y, other.transform.position.z);
        //    ParticleBoosDecrease.Play();
        //    Destroy(other.gameObject);
        /*if (standartSize < localScale.x)
        {
            transform.localScale -= new Vector3(decreaseSizesaw, decreaseSizesaw, decreaseSizesaw);
        }*/

        //if (standartSize >= localScale.x)
        //{
        //Destroy(other.gameObject);
        //}

    }
}


