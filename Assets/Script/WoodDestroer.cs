﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodDestroer : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Cut"))
        {
            Destroy(other.gameObject);
        }

    }


}
